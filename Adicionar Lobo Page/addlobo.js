const sendButton = document.querySelector('.salvar')
const url = 'http://lobinhos.herokuapp.com/wolves'

sendButton.addEventListener('click', () => {
  let nome = document.querySelector('.name').value
  let years = Number(document.querySelector('.years').value)
  let photo = document.querySelector('.photo-url').value
  let description = document.querySelector('#description').value

  if (nome.length > 4 && nome.length < 60) {
    if (isNaN(years) == false) {
      if (years > 0 && years < 100) {
        if (photo.length > 0) {
          if (description.length > 10 && description.length < 255) {
            createWolf(nome, years, photo, description)
          } else {
            alert('A descrição deve conter entre 10 e 255 caracteres.')
          }
        } else {
          alert('O campo de URL da imagem não pode estar vazio.')
        }
      } else {
        alert('Digite uma idade válida entre 0 e 100 anos.')
      }
    } else {
      alert('Digite uma idade válida')
    }
  } else {
    alert('Por favor digite um nome que tenha entre 4 e 60 caracteres.')
  }
})

async function createWolf(nome, years, photo, description) {
  let fetchBody = {
    wolf: {
      name: nome,
      age: Number(years),
      image_url: photo,
      description: description
    }
  }

  let fetchConfig = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(fetchBody)
  }

  await fetch(url, fetchConfig)
    .then(resposta =>
      resposta
        .json()
        .then(resp => {
          console.log(resp)
          alert('Lobo cadastrado com sucesso.')
        })
        .catch(error => {
          console.log(error)
          alert('Ocorreu um erro.')
        })
    )
    .catch(erro => console.log(erro))

  return
}
