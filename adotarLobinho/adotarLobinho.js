const getConfig = {
  method: 'GET'
}
const id = sessionStorage.getItem('idlobo')

function getInformations() {
  let url = `http://lobinhos.herokuapp.com/wolves/${id}/`
  fetch(url, getConfig)
    .then(resposta =>
      resposta
        .json()
        .then(respo => constructor(respo.image_url, respo.name, id))
        .catch(error => console.log(error))
    )
    .catch(erro => console.log(erro))
}

function constructor(photo, name, id) {
  let image = document.querySelector('#imagem')
  let nome = document.querySelector('#name')
  let identificacao = document.querySelector('.id')

  image.src = photo
  nome.innerText = 'Adote o(a) ' + name
  identificacao.innerText = 'ID: ' + id
}

function putAdopt() {
  let name, age, email
  name = document.querySelector('#nome').value
  age = document.querySelector('#idade').value
  email = document.querySelector('#email').value

  if (name == '' || email == '' || age == '') {
    window.alert('Digite valores validos')
  } else {
    let fetchbody = {
      wolf: {
        adopter_name: name,
        adopter_age: Number(age),
        adopter_email: email
      }
    }
    let fetchput = {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(fetchbody)
    }
    let url = `http://lobinhos.herokuapp.com/wolves/${id}/`
    fetch(url, fetchput)
      .then(
        resposta => window.alert('Parabens o lobo acaba de ser adotado'),
        setTimeout(retornar, 3000)
      )
      .catch(erro => console.log(erro))
  }
}

function retornar() {
  window.location.href = '../listaLobinhos/listaLobinhos.html'
  return
}
