const fetchConfig = {
    method: 'GET'
  }
 function getInformations(){
    var id
    id =  sessionStorage.getItem('idlobo')
    let url = `http://lobinhos.herokuapp.com/wolves/${id}/`
    fetch(url, fetchConfig)
    .then(resposta => resposta.json()
        .then(respo => constructor(
                respo.image_url,
                respo.name,
                respo.description)
                )
        .catch(error => console.log(error))
    )
    .catch(erro => console.log(erro))
 }

 function constructor(photo, name, description) {
    let image = document.querySelector('#imagem')
    let nome = document.querySelector('#name')
    let descricao = document.querySelector('.descricao')
  
    image.src = photo
    nome.innerText = name
    descricao.innerText = description
}